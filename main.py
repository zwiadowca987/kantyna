from enum import Enum


class Napoje(Enum):
	CocaCola = 3.00
	Sprite = 2.50
	Lipton = 2.75
	WodaGazowana = 2.00
	WodaNiegazowana = 1.50


class AutomatDoNapojow:
	def __init__(self, cash):
		self.cash = cash
		self.drinks = []
		for drink in Napoje:
			self.drinks.append([drink.name, drink.value, 1])

	def show_client(self):
		for drink in self.drinks:
			print(drink[0], ': ', drink[1], ' zl', sep='')

	def show_worker(self):
		for drink in self.drinks:
			print(drink[0], ': ', drink[1], ' zl (AMOUNT: ', drink[2], ')', sep='')


class Klient:
	def __init__(self, automat):
		self.automat = automat

	def get_data(self):
		self.automat.show_client()

	def buy(self):
		user_input = input('Proszę wybrać napój: ')
		switch = {
			'CocaCola': 0,
			'Sprite': 1,
			'Lipton': 2,
			'WodaGazowana': 3,
			'WodaNiegazowana': 4
		}

		if self.automat.drinks[switch.get(user_input)][2] > 0:
			self.automat.drinks[switch.get(user_input)][2] -= 1
			self.automat.cash += self.automat.drinks[switch.get(user_input)][1]

		else:
			print('Niestety wybrany napój jest czasowo niedostępny.')


class Pracownik:
	def __init__(self, imie, automat):
		self.imie = imie
		self.automat = automat

	def get_data(self):
		self.automat.show_worker()

	def add_drinks(self):
		produkt = input('Proszę podać ilość napojów dodawanych: ').strip().split()

		for i in range(len(self.automat.drinks)):
			self.automat.drinks[i][2] += produkt[i]

		print('Utarg: ', self.automat.cash, ' zł')
		self.automat.cash = 0


class PuszkaNapoju:
	def __init__(self, name, price):
		self.name = name
		self.price = price

	def get_name(self):
		print(self.name)

	def get_price(self):
		print(self.price)


class Kantyna:
	if __name__ == '__main__':
		def klient_menu(self):
			print('Aby wyświetlić listę dostępnych produktów wciśnij 1')
			print('Aby dokonać zakupu wciśnij 2')
			print('Aby anulować wciśnij 0')
			user_input = input()

			if user_input == 1:
				self.klient.get_data()
			elif user_input == 2:
				self.klient.buy()
			else:
				return 0

		def pracownik_menu(self):
			print('Aby dołożyć napoje i zebrać utarg wciśnij 1')
			print('Aby sprawdzić zawartość automatu wciśnij 2')
			print('Aby anulować wciśnij 0')
			user_input = input()

			if user_input == 1:
				self.pracownik.get_data()
			elif user_input == 2:
				self.pracownik.add_drinks()
			else:
				return 0

		automat = AutomatDoNapojow(20)
		klient = Klient(automat)
		pracownik = Pracownik('imie', 40)
		menu_input = 0

		while menu_input == 0:
			print('To jest aplikacja do obsługi automatu z napojami w kantynie')
			print('Aby przejść do menu klienta wciśnij 1')
			print('Aby przejść do menu pracownika wciśnij 2')
			print('Aby wyjść z programu wciśnij 0')

			menu_input = input()

			if menu_input == 1:
				klient_menu()
			elif menu_input == 2:
				pracownik_menu()
			elif not menu_input == 0:
				print('Proszę wprowadzić prawidłową komendę')
